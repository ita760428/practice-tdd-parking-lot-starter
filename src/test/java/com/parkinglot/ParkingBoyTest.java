package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_ticket_from_the_first_parkingLot_when_park_a_car_given_a_car_and_two_parkLot_and_a_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.getTicketCar().containsKey(ticket));
    }

    @Test
    void should_return_a_ticket_from_the_second_parkingLot_when_park_a_car_given_a_car_and_two_parkLot_the_first_parkingLot_is_full_and_a_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);

        //when
        Ticket ticket2 = parkingBoy.park(car2);

        //then
        Assertions.assertTrue(parkingLot2.getTicketCar().containsKey(ticket2));
    }

    @Test
    void should_return_right_car_when_fetch_car_given_two_ticket_from_different_parkingLot_and_two_parkingLot_and_a_parkingBoy_and_two_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        //when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }


    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_wrong_ticket_and_parkingLots_parked_car_and_a_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        Ticket wrongTicket = new Ticket();

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> parkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());

    }
    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_used_ticket_and_packingLots_parked_car_and_a_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        Car fetchCar = parkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_message_No_available_position_when_park_car_given_tow_parkLot_without_position_and_a_car_and_a_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingBoy.setParkingLotList(parkingLotList);
        parkingBoy.park(car1);
        parkingBoy.park(car2);

        //when then
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class,
                ()-> parkingBoy.park(car3));
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());



    }
}
