package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_ticket_when_park_a_car_given_a_car_and_a_parkingLot(){
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_a_ticket_and_a_parkingLot_parked_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.park(car);

        //when
        Car carFetched = parkingLot.fetch(ticket);

        //then
        Assertions.assertEquals(car, carFetched);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_two_ticket_and_parkingLot_parked_car() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        //when
        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);

    }

    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_wrong_ticket_and_parkingLot_parked_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.park(car);
        Ticket wrongTicket = new Ticket();

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> parkingLot.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());

    }

    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_used_ticket_and_packingLot_parked_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Car fetchCar = parkingLot.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_No_available_position_when_park_car_given_a_parkLot_without_position() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        Car car = new Car();

        ///when then
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class,
                ()-> parkingLot.park(car));
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }
}
