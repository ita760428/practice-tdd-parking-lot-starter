package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_a_ticket_from_the_spaciest_rate_parkingLot_when_park_a_car_given_a_car_and_two_parkLot_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLot1.park(car1);
        parkingLot1.park(car2);


        //when
        Ticket ticket = superSmartParkingBoy.park(car2);

        //then
        Assertions.assertTrue(parkingLot2.getTicketCar().containsKey(ticket));
    }

    @Test
    void should_return_the_parked_car_when_fetch_a_car_given_a_ticket_and_two_parkingLot_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);


        //when
        Car fetchCar = superSmartParkingBoy.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchCar);
    }

    @Test
    void should_return_the_right_car_when_fetch_car_given_two_ticket_and_two_parkLot_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        //when
        Car fetchCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchCar2 = superSmartParkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_wrong_ticket_and_parkingLots_parked_car_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);
        Ticket wrongTicket = new Ticket();

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> superSmartParkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());

    }
    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fetch_car_given_a_used_ticket_and_parkingLots_parked_car_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);
        Car  fetchCar = superSmartParkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class,
                ()-> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedTicketException.getMessage());

    }

    @Test
    void should_return_message_No_available_position_when_park_car_given_tow_parkLot_without_position_and_a_car_and_a_superSmartParkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        superSmartParkingBoy.setParkingLotList(parkingLotList);
        superSmartParkingBoy.park(car1);
        superSmartParkingBoy.park(car2);
        superSmartParkingBoy.park(car3);

        //when then
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class,
                ()-> superSmartParkingBoy.park(car4));
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }
}
