package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy {

    private List<ParkingLot> parkingLotList = new ArrayList<>();

    public void setParkingLotList(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }

    public Ticket park(Car car) {
        ParkingLot priorParkingLot = null;
        int maxContain = 0;
        int flag = 0;
        for( ParkingLot parkingLot: this.parkingLotList){
            int remainingContain = parkingLot.getMaxSize()-parkingLot.getTicketMapSize();
            if(remainingContain > maxContain){
                maxContain = remainingContain;
                priorParkingLot = parkingLot;
                flag = 1;
            }
        }
        if(flag == 0){
            throw new NoAvailableException();
        }
        return priorParkingLot.park(car);
    }

    public Car fetch(Ticket ticket) {
        Car car = null;
        int flag =0;
        for(ParkingLot parkingLot : this.parkingLotList){
            if(parkingLot.getTicketCar().containsKey(ticket)){
                car = parkingLot.fetch(ticket);
                flag =1;
                break;
            }
        }
        if(flag==0){
            throw new UnrecognizedTicketException();
        }
        return car;
    }
}
