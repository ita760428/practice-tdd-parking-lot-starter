package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private int maxSize;

    public ParkingLot(int maxSize) {
        this.maxSize = maxSize;
    }

    private Map<Ticket,Car> ticketCar = new HashMap<>();
    public Ticket park(Car car) {
        if(this.ticketCar.size()==maxSize){
            throw new NoAvailableException();
        }
        Ticket ticket = new Ticket();
        this.ticketCar.put(ticket,car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        Car car = this.ticketCar.get(ticket);
        if(car == null){
            throw new UnrecognizedTicketException();
        }
        this.ticketCar.remove(ticket);
        return car;
    }

    public int getTicketMapSize(){
        return this.ticketCar.size();
    }

    public Map<Ticket, Car> getTicketCar() {
        return ticketCar;
    }

    public int getMaxSize() {
        return maxSize;
    }
}
