package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoy {
    private List<ParkingLot> parkingLotList = new ArrayList<>();
    public void setParkingLotList(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }

    public Ticket park(Car car) {
        ParkingLot priorParkingLot = null;
        double maxContainRate = 0;
        int flag = 0;
        for( ParkingLot parkingLot: this.parkingLotList){
            double remainingContainRate = (parkingLot.getMaxSize()-parkingLot.getTicketMapSize())*1.0/parkingLot.getMaxSize();
            if( remainingContainRate> maxContainRate){
                maxContainRate = remainingContainRate;
                priorParkingLot = parkingLot;
                flag = 1;
            }
        }
        if(flag == 0){
            throw new NoAvailableException();
        }
        return priorParkingLot.park(car);
    }

    public Car fetch(Ticket ticket) {
        Car car = null;
        int flag =0;
        for(ParkingLot parkingLot : this.parkingLotList){
            if(parkingLot.getTicketCar().containsKey(ticket)){
                car = parkingLot.fetch(ticket);
                flag =1;
                break;
            }
        }
        if(flag==0){
            throw new UnrecognizedTicketException();
        }
        return car;
    }
}
