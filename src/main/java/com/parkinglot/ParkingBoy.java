package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    private List<ParkingLot> parkingLotList = new ArrayList<>();


    public Ticket park(Car car) {
        Ticket ticket = null;
        int flag = 0;
        for(ParkingLot parkingLot : parkingLotList){
            if(parkingLot.getTicketMapSize()<parkingLot.getMaxSize()){
                ticket= parkingLot.park(car);
                flag = 1;
                break;
            }
        }
        if(flag == 0){
            throw new NoAvailableException();
        }else{
            return ticket;
        }

    }

    public Car fetch(Ticket ticket) {
        Car car = null;
        int flag =0;
        for(ParkingLot parkingLot : parkingLotList){
            if(parkingLot.getTicketCar().containsKey(ticket)){
                car = parkingLot.fetch(ticket);
                flag =1;
                break;
            }
        }
        if(flag==0){
            throw new UnrecognizedTicketException();
        }
        return car;
    }

    public void setParkingLotList(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }
}
